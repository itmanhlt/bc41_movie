import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localUserServ } from "../../services/localService";

export default function UserMenu() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  let handleLogout = () => {
    localUserServ.remove();
    // window.location.href = "/login";
    window.location.reload();
  };
  let renderUserInfo = () => {
    if (userInfo) {
      return (
        <>
          <span>{userInfo.hoTen}</span>
          <button
            onClick={handleLogout}
            className="px-5 py-2 rounded border-2 border-black"
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className="px-5 py-2 rounded border-2 border-black">
              Đăng nhập
            </button>
          </NavLink>
          <NavLink to="/sign-up">
            <button className="px-5 py-2 rounded border-2 border-black">
              Đăng ký
            </button>
          </NavLink>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderUserInfo()}</div>;
}
