import React from "react";

import { Card, Button } from "antd";
const { Meta } = Card;
const ItemMovie = ({ data }) => (
  <Card
    hoverable
    style={{
      width: "100%",
    }}
    cover={
      <img
        className="h-60 object-cover object-top"
        alt="example"
        src={data.hinhAnh}
      />
    }
  >
    <Meta title={data.tenPhim} />
    <Button danger type="primary">
      xem ngay
    </Button>
  </Card>
);
export default ItemMovie;
