import React, { useEffect, useState } from "react";
import { movieServ } from "../../../services/movieService";
import ItemMovie from "./ItemMovie";

export default function ListMovie() {
  const movieArr = [];
  const [movieList, setMovieList] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieList()
      .then((res) => {
        setMovieList(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="grid grid-cols-4 gap-10">
      {movieList.slice(0, 8).map((movie) => {
        return <ItemMovie data={movie} key={movie.maPhim} />;
      })}
    </div>
  );
}
