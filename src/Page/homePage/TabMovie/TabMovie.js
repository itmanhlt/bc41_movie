import React, { useEffect, useState } from "react";
import { movieServ } from "../../../services/movieService";

import { Tabs } from "antd";
import ItemTabMovie from "./ItemTabMovie";
const onChange = (key) => {
  console.log(key);
};
const items = [
  {
    key: "1",
    label: `Tab 1`,
    children: `Content of Tab Pane 1`,
  },
  //   {
  //     key: "2",
  //     label: `Tab 2`,
  //     children: `Content of Tab Pane 2`,
  //   },
  //   {
  //     key: "3",
  //     label: `Tab 3`,
  //     children: `Content of Tab Pane 3`,
  //   },
];
export default function TabMovie() {
  const [danhSachHeThongRap, setdanhSachHeThongRap] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieTheater()
      .then((res) => {
        console.log(res.data);
        setdanhSachHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return danhSachHeThongRap.map((heThongRap) => {
      return {
        key: heThongRap.maHeThongRap,
        label: <img className="h-16" src={heThongRap.logo} alt="" />,
        children: (
          <Tabs
            style={{ height: 700 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className="w-60 truncate">
                    <p className="font-medium">{cumRap.tenCumRap}</p>
                    <p className="text-xs text-gray-600">{cumRap.diaChi}</p>
                  </div>
                ),
                children: (
                  <div
                    style={{ height: 700, overflowY: "scroll" }}
                    className="space-y-5"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMovie key={phim.maPhim} phim={phim} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };
  return (
    <div>
      <Tabs
        style={{ height: 700 }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
