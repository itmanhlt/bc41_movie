import moment from "moment/moment";
import React from "react";

export default function ItemTabMovie({ phim }) {
  return (
    <div className="flex space-x-5 border-b pb-3">
      <img className="w-28 h-36" src={phim.hinhAnh} alt="" />
      <div>
        <h5 className="font-medium text-xl text-red-600">{phim.tenPhim}</h5>
        <div className="grid grid-cols-2 gap-4">
          {phim.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
            return (
              <div
                key={item.maLichChieu}
                className="border-2 p-2 bg-orange-100"
              >
                {moment(item.ngayChieuGioChieu).format("DD-mm-yyyy ~ h:mm")}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
