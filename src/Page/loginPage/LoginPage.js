import React from "react";
import { Button, Form, Input, message } from "antd";
import { userServ } from "../../services/useService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_LOGIN } from "../../redux/constant/userConstant";
import { localUserServ } from "../../services/localService";
import Lottie from "lottie-react";
import animate from "../../asset/animate.json";

const LoginPage = () => {
  let navigate = useNavigate();
  let disptach = useDispatch();

  const onFinish = (values) => {
    console.log("Success:", values);
    userServ
      .login(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        disptach({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        localUserServ.set(res.data.content);
        navigate("/");
        console.log(res.data.content);
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen p-20 bg-orange-500 flex justify-center items-center">
      <div className="container  p-20 bg-white rounded-lg flex ">
        <div className="w-1/2 h-full ">
          <Lottie style={{ height: 300 }} animationData={animate} loop={true} />
        </div>
        <div className="w-1/2 h-full ">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            className="w-full"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button danger type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
