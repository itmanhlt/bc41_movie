import { localUserServ } from "../services/localService";
import { SET_USER_LOGIN } from "./constant/userConstant";

let initialState = {
  userInfo: localUserServ.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_LOGIN: {
      return { ...state, userInfo: payload };
    }
    default:
      return state;
  }
};
